package io.microservice.quiz.flow.quizsheet.api.command.command;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TakeQuizCommand extends CqrsUserCommand {
    //
    private String quizSheetId;
    private IdName writer;
}
