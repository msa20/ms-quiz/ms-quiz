package io.microservice.quiz.flow.quizsheet.api.command.command;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizAnswerNameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ModifyQuizAnswersCommand extends CqrsUserCommand {
    //
    private List<QuizAnswerNameValueList> nameValueLists;
}
