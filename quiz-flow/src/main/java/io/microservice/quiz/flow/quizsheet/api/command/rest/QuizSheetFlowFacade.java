/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.flow.quizsheet.api.command.rest;

import io.microservice.quiz.flow.quizsheet.api.command.command.ModifyQuizAnswersCommand;
import io.microservice.quiz.flow.quizsheet.api.command.command.TakeQuizCommand;

public interface QuizSheetFlowFacade {
    //
    TakeQuizCommand takeQuiz(TakeQuizCommand command);
    ModifyQuizAnswersCommand modifyQuizAnswers(ModifyQuizAnswersCommand command);
}
