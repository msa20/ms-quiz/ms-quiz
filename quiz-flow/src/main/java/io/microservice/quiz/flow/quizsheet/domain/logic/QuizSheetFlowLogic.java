package io.microservice.quiz.flow.quizsheet.domain.logic;


import io.microservice.quiz.aggregate.quizsheet.domain.entity.Quiz;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizSheet;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizAnswerCdo;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizAnswerSheetCdo;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizGradingResult;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizAnswerNameValueList;
import io.microservice.quiz.aggregate.quizsheet.domain.logic.QuizAnswerLogic;
import io.microservice.quiz.aggregate.quizsheet.domain.logic.QuizAnswerSheetLogic;
import io.microservice.quiz.aggregate.quizsheet.domain.logic.QuizLogic;
import io.microservice.quiz.aggregate.quizsheet.domain.logic.QuizSheetLogic;
import io.microservice.quiz.flow.quizsheet.api.command.command.ModifyQuizAnswersCommand;
import io.microservice.quiz.flow.quizsheet.api.command.command.TakeQuizCommand;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QuizSheetFlowLogic {
    //
    private final QuizSheetLogic quizSheetLogic;
    private final QuizAnswerSheetLogic quizAnswerSheetLogic;
    private final QuizLogic quizLogic;
    private final QuizAnswerLogic quizAnswerLogic;

    public TakeQuizCommand takeQuiz(TakeQuizCommand command) {
        //
        String quizSheetId = command.getQuizSheetId();
        IdName writer = command.getWriter();
        QuizSheet quizSheet = quizSheetLogic.findQuizSheet(quizSheetId);

        QuizAnswerSheetCdo cdo = new QuizAnswerSheetCdo();
        cdo.setGroupId(quizSheet.getGroupId());
        cdo.setQuizSheetId(quizSheet.getId());
        cdo.setWriter(writer);
        cdo.setSubject(quizSheet.getSubject());

        String quizAnswerSheetId = quizAnswerSheetLogic.registerQuizAnswerSheet(cdo);

        List<Quiz> quizzes = quizLogic.findQuizByQuizSheetId(quizSheetId);

        List<QuizAnswerCdo> quizAnswerCdos = quizzes.stream().map(quiz -> {
            QuizAnswerCdo quizAnswerCdo = new QuizAnswerCdo();
            quizAnswerCdo.setQuizAnswerSheetId(quizAnswerSheetId);
            quizAnswerCdo.setQuizId(quiz.getId());
            quizAnswerCdo.setQuizGradingResult(QuizGradingResult.InCorrect);
            quizAnswerCdo.setWriter(writer);

            return quizAnswerCdo;
        }).collect(Collectors.toList());

        quizAnswerLogic.registerQuizAnswers(quizAnswerCdos);

        command.setCommandResponse(new CommandResponse(quizAnswerSheetId));
        return command;
    }

    public ModifyQuizAnswersCommand modifyQuizAnswers(ModifyQuizAnswersCommand command) {
        //
        List<QuizAnswerNameValueList> quizAnswerNameValueLists = command.getNameValueLists();

        List<String> modifiedQuizAnswerIds = quizAnswerNameValueLists.stream().map(quizAnswerNameValueList -> {
            String targetQuizAnswerId = quizAnswerNameValueList.getQuizAnswerId();
            NameValueList nameValueList = quizAnswerNameValueList.getNameValueList();

            quizAnswerLogic.modifyQuizAnswer(targetQuizAnswerId, nameValueList);

            return targetQuizAnswerId;
        }).collect(Collectors.toList());


        command.setCommandResponse(new CommandResponse(modifiedQuizAnswerIds));

        return command;
    }
}
