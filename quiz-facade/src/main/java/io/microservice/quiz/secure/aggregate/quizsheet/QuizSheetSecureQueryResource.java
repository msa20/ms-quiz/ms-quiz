/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.secure.aggregate.quizsheet;

import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizSheetDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizSheetQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizSheetsDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.rest.QuizSheetQueryFacade;
import io.microservice.quiz.aggregate.quizsheet.store.QuizSheetStore;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizSheetJpo;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;

@RestController
@RequestMapping("/secure/quiz-sheet/query")
public class QuizSheetSecureQueryResource implements QuizSheetQueryFacade {
    /* Autogen by nara studio */
    private final QuizSheetStore quizSheetStore;
    private final RdbQueryRequest<QuizSheetJpo> request;

    public QuizSheetSecureQueryResource(QuizSheetStore quizSheetStore, EntityManager entityManager) {
        /* Autogen by nara studio */
        this.quizSheetStore = quizSheetStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QuizSheetQuery execute(@RequestBody QuizSheetQuery quizSheetQuery) {
        /* Autogen by nara studio */
        quizSheetQuery.execute(quizSheetStore);
        return quizSheetQuery;
    }

    @Override
    @PostMapping("/dynamic-single")
    public QuizSheetDynamicQuery execute(@RequestBody QuizSheetDynamicQuery quizSheetDynamicQuery) {
        /* Autogen by nara studio */
        quizSheetDynamicQuery.execute(request);
        return quizSheetDynamicQuery;
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QuizSheetsDynamicQuery execute(@RequestBody QuizSheetsDynamicQuery quizSheetsDynamicQuery) {
        /* Autogen by nara studio */
        quizSheetsDynamicQuery.execute(request);
        return quizSheetsDynamicQuery;
    }
}
