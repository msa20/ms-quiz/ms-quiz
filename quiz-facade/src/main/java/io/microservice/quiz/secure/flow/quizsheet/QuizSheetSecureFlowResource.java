/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.secure.flow.quizsheet;

import io.microservice.quiz.flow.quizsheet.api.command.command.ModifyQuizAnswersCommand;
import io.microservice.quiz.flow.quizsheet.api.command.command.TakeQuizCommand;
import io.microservice.quiz.flow.quizsheet.api.command.rest.QuizSheetFlowFacade;
import io.microservice.quiz.flow.quizsheet.domain.logic.QuizSheetFlowLogic;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/secure/question-flow")
@RequiredArgsConstructor
public class QuizSheetSecureFlowResource implements QuizSheetFlowFacade {
    //
    private final QuizSheetFlowLogic quizSheetFlowLogic;

    @Override
    @PostMapping("/take-quiz")
    public TakeQuizCommand takeQuiz(@RequestBody TakeQuizCommand command) {
        //
        return quizSheetFlowLogic.takeQuiz(command);
    }

    @Override
    @PostMapping("/modify-quiz-answers")
    public ModifyQuizAnswersCommand modifyQuizAnswers(@RequestBody ModifyQuizAnswersCommand command) {
        //
        return quizSheetFlowLogic.modifyQuizAnswers(command);
    }
}
