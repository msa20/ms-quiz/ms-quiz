/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.secure.aggregate.quizsheet;

import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswersDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.rest.QuizAnswerQueryFacade;
import io.microservice.quiz.aggregate.quizsheet.store.QuizAnswerStore;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizAnswerJpo;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;

@RestController
@RequestMapping("/secure/quiz-answer/query")
public class QuizAnswerSecureQueryResource implements QuizAnswerQueryFacade {
    /* Autogen by nara studio */
    private final QuizAnswerStore quizAnswerStore;
    private final RdbQueryRequest<QuizAnswerJpo> request;

    public QuizAnswerSecureQueryResource(QuizAnswerStore quizAnswerStore, EntityManager entityManager) {
        /* Autogen by nara studio */
        this.quizAnswerStore = quizAnswerStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QuizAnswerQuery execute(@RequestBody QuizAnswerQuery quizAnswerQuery) {
        /* Autogen by nara studio */
        quizAnswerQuery.execute(quizAnswerStore);
        return quizAnswerQuery;
    }

    @Override
    @PostMapping("/dynamic-single")
    public QuizAnswerDynamicQuery execute(@RequestBody QuizAnswerDynamicQuery quizAnswerDynamicQuery) {
        /* Autogen by nara studio */
        quizAnswerDynamicQuery.execute(request);
        return quizAnswerDynamicQuery;
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QuizAnswersDynamicQuery execute(@RequestBody QuizAnswersDynamicQuery quizAnswersDynamicQuery) {
        /* Autogen by nara studio */
        quizAnswersDynamicQuery.execute(request);
        return quizAnswersDynamicQuery;
    }
}
