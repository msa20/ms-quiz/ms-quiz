/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.secure.aggregate.quizsheet;

import io.microservice.quiz.aggregate.quizsheet.api.command.command.*;
import io.microservice.quiz.aggregate.quizsheet.api.command.rest.QuizSheetFacade;
import io.microservice.quiz.aggregate.quizsheet.domain.logic.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/secure/quiz-sheet")
public class QuizSheetSecureResource implements QuizSheetFacade {
    /* Autogen by nara studio */
    private final QuizLogic quizLogic;
    private final QuizSheetLogic quizSheetLogic;
    private final QuizAnswerLogic quizAnswerLogic;
    private final QuizAnswerSheetLogic quizAnswerSheetLogic;

    public QuizSheetSecureResource(
            QuizLogic quizLogic,
            QuizSheetLogic quizSheetLogic,
            QuizAnswerLogic quizAnswerLogic,
            QuizAnswerSheetLogic quizAnswerSheetLogic
    ) {
        /* Autogen by nara studio */
        this.quizLogic = quizLogic;
        this.quizSheetLogic = quizSheetLogic;
        this.quizAnswerLogic = quizAnswerLogic;
        this.quizAnswerSheetLogic = quizAnswerSheetLogic;
    }

    @Override
    @PostMapping("/quiz/command")
    public QuizCommand executeQuiz(@RequestBody QuizCommand quizCommand) {
        /* Autogen by nara studio */
        return quizLogic.routeCommand(quizCommand);
    }

    @Override
    @PostMapping("/quiz-sheet/command")
    public QuizSheetCommand executeQuizSheet(@RequestBody QuizSheetCommand quizSheetCommand) {
        /* Autogen by nara studio */
        return quizSheetLogic.routeCommand(quizSheetCommand);
    }

    @Override
    @PostMapping("/quiz-answer/command")
    public QuizAnswerCommand executeQuizAnswer(@RequestBody QuizAnswerCommand quizAnswerCommand) {
        /* Autogen by nara studio */
        return quizAnswerLogic.routeCommand(quizAnswerCommand);
    }

    @Override
    @PostMapping("/quiz-answer-sheet/command")
    public QuizAnswerSheetCommand executeQuizAnswerSheet(@RequestBody QuizAnswerSheetCommand quizAnswerSheetCommand) {
        /* Autogen by nara studio */
        return quizAnswerSheetLogic.routeCommand(quizAnswerSheetCommand);
    }
}
