/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.event.projection;

import io.microservice.quiz.aggregate.quizsheet.domain.event.*;
import io.microservice.quiz.aggregate.quizsheet.domain.logic.*;
import io.naradrama.prologue.domain.cqrs.broker.StreamEventMessage;

public class ProjectionHandler {
    private final QuizLogic quizLogic; // Autogen by nara studio
    private final QuizAnswerLogic quizAnswerLogic;
    private final QuizAnswerSheetLogic quizAnswerSheetLogic;
    private final QuizSheetLogic quizSheetLogic;

    public ProjectionHandler(QuizLogic quizLogic, QuizAnswerLogic quizAnswerLogic, QuizAnswerSheetLogic quizAnswerSheetLogic, QuizSheetLogic quizSheetLogic) {
        /* Autogen by nara studio */
        this.quizLogic = quizLogic;
        this.quizAnswerLogic = quizAnswerLogic;
        this.quizAnswerSheetLogic = quizAnswerSheetLogic;
        this.quizSheetLogic = quizSheetLogic;
    }

    public void handle(StreamEventMessage streamEventMessage) {
        /* Autogen by nara studio */
        String classFullName = streamEventMessage.getPayloadClass();
        String payload = streamEventMessage.getPayload();
        String eventName = classFullName.substring(classFullName.lastIndexOf(".") + 1);
        switch(eventName) {
            case "QuizEvent":
                QuizEvent quizEvent = QuizEvent.fromJson(payload);
                quizLogic.handleEventForProjection(quizEvent);
                break;
            case "QuizAnswerEvent":
                QuizAnswerEvent quizAnswerEvent = QuizAnswerEvent.fromJson(payload);
                quizAnswerLogic.handleEventForProjection(quizAnswerEvent);
                break;
            case "QuizAnswerSheetEvent":
                QuizAnswerSheetEvent quizAnswerSheetEvent = QuizAnswerSheetEvent.fromJson(payload);
                quizAnswerSheetLogic.handleEventForProjection(quizAnswerSheetEvent);
                break;
            case "QuizSheetEvent":
                QuizSheetEvent quizSheetEvent = QuizSheetEvent.fromJson(payload);
                quizSheetLogic.handleEventForProjection(quizSheetEvent);
                break;
        }
    }
}
