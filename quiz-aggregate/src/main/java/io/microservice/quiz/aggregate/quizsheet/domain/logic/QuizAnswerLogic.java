/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.domain.logic;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import io.microservice.quiz.aggregate.quizsheet.store.QuizAnswerStore;
import io.microservice.quiz.aggregate.quizsheet.api.command.command.QuizAnswerCommand;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import java.lang.String;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizAnswerCdo;
import java.util.NoSuchElementException;
import io.microservice.quiz.aggregate.quizsheet.domain.event.QuizAnswerEvent;
import org.springframework.context.ApplicationEventPublisher;
import java.util.List;
import java.util.stream.Collectors;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswer;
import io.naradrama.prologue.domain.Offset;
import io.naradrama.prologue.domain.NameValueList;

@Service
@Transactional
public class QuizAnswerLogic {
    /* Autogen by nara studio */
    private final QuizAnswerStore quizAnswerStore;
    private final ApplicationEventPublisher eventPublisher;

    public QuizAnswerLogic(QuizAnswerStore quizAnswerStore, ApplicationEventPublisher eventPublisher) {
        /* Autogen by nara studio */
        this.quizAnswerStore = quizAnswerStore;
        this.eventPublisher = eventPublisher;
    }

    public QuizAnswerCommand routeCommand(QuizAnswerCommand command) {
        /* Autogen by nara studio */
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.getQuizAnswerCdos().size() > 0) {
                    List<String> entityIds = this.registerQuizAnswers(command.getQuizAnswerCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = this.registerQuizAnswer(command.getQuizAnswerCdo());
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyQuizAnswer(command.getQuizAnswerId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getQuizAnswerId()));
                break;
            case Remove:
                this.removeQuizAnswer(command.getQuizAnswerId());
                command.setCommandResponse(new CommandResponse(command.getQuizAnswerId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerQuizAnswer(QuizAnswerCdo quizAnswerCdo) {
        /* Autogen by nara studio */
        QuizAnswer quizAnswer = new QuizAnswer(quizAnswerCdo);
        if (quizAnswerStore.exists(quizAnswer.getId())) {
            throw new IllegalArgumentException("quizAnswer already exists. " + quizAnswer.getId());
        }
        quizAnswerStore.create(quizAnswer);
        QuizAnswerEvent quizAnswerEvent = QuizAnswerEvent.newQuizAnswerRegisteredEvent(quizAnswer);
        eventPublisher.publishEvent(quizAnswerEvent);
        return quizAnswer.getId();
    }

    public List<String> registerQuizAnswers(List<QuizAnswerCdo> quizAnswerCdos) {
        /* Autogen by nara studio */
        return quizAnswerCdos.stream().map(quizAnswerCdo -> this.registerQuizAnswer(quizAnswerCdo)).collect(Collectors.toList());
    }

    public QuizAnswer findQuizAnswer(String quizAnswerId) {
        /* Autogen by nara studio */
        QuizAnswer quizAnswer = quizAnswerStore.retrieve(quizAnswerId);
        if (quizAnswer == null) {
            throw new NoSuchElementException("QuizAnswer id: " + quizAnswerId);
        }
        return quizAnswer;
    }

    public List<QuizAnswer> findAllQuizAnswer(Offset offset) {
        /* Autogen by nara studio */
        return quizAnswerStore.retrieveAll(offset);
    }

    public void modifyQuizAnswer(String quizAnswerId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizAnswer quizAnswer = findQuizAnswer(quizAnswerId);
        quizAnswer.modifyValues(nameValues);
        quizAnswerStore.update(quizAnswer);
        QuizAnswerEvent quizAnswerEvent = QuizAnswerEvent.newQuizAnswerModifiedEvent(quizAnswerId, nameValues);
        eventPublisher.publishEvent(quizAnswerEvent);
    }

    public void modifyQuizAnswer(QuizAnswer quizAnswer) {
        // Check existence
        QuizAnswer foundQuizAnswer = findQuizAnswer(quizAnswer.getId());
        quizAnswerStore.update(quizAnswer);
        QuizAnswerEvent quizAnswerEvent = QuizAnswerEvent.newQuizAnswerModifiedEvent(quizAnswer);
        eventPublisher.publishEvent(quizAnswerEvent);
    }

    public void removeQuizAnswer(String quizAnswerId) {
        /* Autogen by nara studio */
        QuizAnswer quizAnswer = findQuizAnswer(quizAnswerId);
        quizAnswerStore.delete(quizAnswer);
        QuizAnswerEvent quizAnswerEvent = QuizAnswerEvent.newQuizAnswerRemovedEvent(quizAnswer);
        eventPublisher.publishEvent(quizAnswerEvent);
    }

    public boolean existsQuizAnswer(String quizAnswerId) {
        /* Autogen by nara studio */
        return quizAnswerStore.exists(quizAnswerId);
    }

    public void handleEventForProjection(QuizAnswerEvent quizAnswerEvent) {
        /* Autogen by nara studio */
        switch(quizAnswerEvent.getCqrsDataEventType()) {
            case Registered:
                quizAnswerStore.create(quizAnswerEvent.getQuizAnswer());
                break;
            case Modified:
                QuizAnswer quizAnswer = quizAnswerStore.retrieve(quizAnswerEvent.getQuizAnswerId());
                quizAnswer.modifyValues(quizAnswerEvent.getNameValues());
                quizAnswerStore.update(quizAnswer);
                break;
            case Removed:
                quizAnswerStore.delete(quizAnswerEvent.getQuizAnswerId());
                break;
        }
    }
}
