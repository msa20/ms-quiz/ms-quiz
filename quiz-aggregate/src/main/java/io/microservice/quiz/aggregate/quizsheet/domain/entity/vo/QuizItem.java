package io.microservice.quiz.aggregate.quizsheet.domain.entity.vo;

import io.naradrama.prologue.domain.ddd.ValueObject;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class QuizItem implements ValueObject {
    //
    private int sequence;
    private String itemText;

    @Override
    public String toString() {
        return toJson();
    }

    public static QuizItem fromJson(String json) {
        //
        return JsonUtil.fromJson(json, QuizItem.class);
    }
}
