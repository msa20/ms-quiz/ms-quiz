package io.microservice.quiz.aggregate.quizsheet.domain.entity;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizCdo;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizCategory;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizDifficultyLevel;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizItem;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizState;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.Enum;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Quiz extends DomainEntity {
    private String groupId;
    private String quizSheetId;

    private IdName writer;
    private String no;
    private String text;
    private String subText;
    private QuizCategory quizCategory;
    @Enum
    private QuizDifficultyLevel quizDifficultyLevel;
    @Enum
    private QuizState quizState;
    private String correctSeq;
    private List<QuizItem> quizItems;

    transient private List<QuizAnswer> quizAnswers;

    public Quiz(String id){
        super(id);
    }

    public Quiz(QuizCdo quizCdo){
        super();
        BeanUtils.copyProperties(quizCdo,this);
    }

    public void modifyValues(NameValueList nameValues) {
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();

            switch (nameValue.getName()){
                case "no":
                    this.no = value;
                    break;
                case "text":
                    this.text = value;
                    break;
                case "subText":
                    this.subText = value;
                    break;
                case "quizCategory":
                    this.quizCategory = QuizCategory.valueOf(value);
                    break;
                case "quizDifficultyLevel":
                    this.quizDifficultyLevel = QuizDifficultyLevel.valueOf(value);
                    break;
                case "quizState":
                    this.quizState = QuizState.valueOf(value);
                    break;
                case "correctSeq":
                    this.correctSeq = value;
                    break;
                case "quizItems":
                    this.quizItems = JsonUtil.fromJsonList(value, QuizItem.class);
                    break;
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }
}
