package io.microservice.quiz.aggregate.quizsheet.domain.entity;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizSheetCdo;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizSheetCategory;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizSheetState;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainAggregate;
import io.naradrama.prologue.domain.ddd.Enum;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class QuizSheet extends DomainEntity implements DomainAggregate {

    private String groupId;

    private String title;
    private String subject;
    private IdName writer;
    @Enum
    private QuizSheetCategory quizSheetCategory;

    private QuizSheetState quizSheetState;


    transient private List<Quiz> quizzes;
    transient private List<QuizAnswerSheet> quizAnswerSheets;

    public QuizSheet(String id){
        super(id);
    }

    public QuizSheet(QuizSheetCdo quizSheetCdo){
        super();
        BeanUtils.copyProperties(quizSheetCdo,this);
    }

    public void modifyValues(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();

            switch (nameValue.getName()){
                case "title":
                    this.title = value;
                    break;
                case "subject":
                    this.subject = value;
                    break;
                case "quizSheetCategory":
                    this.quizSheetCategory = QuizSheetCategory.valueOf(value);
                    break;
                case "quizSheetState":
                    this.quizSheetState = QuizSheetState.valueOf(value);
                    break;
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }
}
