/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store.maria.jpo;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizItem;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.store.jpa.DomainEntityJpo;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.Quiz;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizCategory;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizDifficultyLevel;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizState;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "QUIZ")
public class QuizJpo extends DomainEntityJpo {
    /* Autogen by nara studio */
    private String groupId;
    private String quizSheetId;
    private String writerId;
    private String writerName;
    private String no;
    private String text;
    private String subText;
    @Enumerated(EnumType.STRING)
    private QuizCategory quizCategory;
    @Enumerated(EnumType.STRING)
    private QuizDifficultyLevel quizDifficultyLevel;
    @Enumerated(EnumType.STRING)
    private QuizState quizState;
    private String correctSeq;
    private String quizItemsJson;

    public QuizJpo(Quiz quiz) {
        /* Autogen by nara studio */
        super(quiz);
        BeanUtils.copyProperties(quiz, this);

        this.writerId = quiz.getWriter().getId();
        this.writerName = quiz.getWriter().getName();
        this.quizItemsJson = JsonUtil.toJson(quiz.getQuizItems(), true);
    }

    public Quiz toDomain() {
        /* Autogen by nara studio */
        Quiz quiz = new Quiz(getId());
        BeanUtils.copyProperties(this, quiz);

        quiz.setWriter(IdName.of(this.writerId, this.writerName));
        quiz.setQuizItems(JsonUtil.fromJsonList(this.quizItemsJson, QuizItem.class));
        return quiz;
    }

    public static List<Quiz> toDomains(List<QuizJpo> quizJpos) {
        /* Autogen by nara studio */
        return quizJpos.stream().map(QuizJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
