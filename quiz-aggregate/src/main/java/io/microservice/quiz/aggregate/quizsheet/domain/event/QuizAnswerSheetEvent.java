/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.domain.event;

import lombok.Getter;
import lombok.Setter;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswerSheet;
import java.lang.String;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
public class QuizAnswerSheetEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private QuizAnswerSheet quizAnswerSheet;
    private String quizAnswerSheetId;
    private NameValueList nameValues;

    protected QuizAnswerSheetEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static QuizAnswerSheetEvent newQuizAnswerSheetRegisteredEvent(QuizAnswerSheet quizAnswerSheet) {
        /* Autogen by nara studio */
        QuizAnswerSheetEvent event = new QuizAnswerSheetEvent(CqrsDataEventType.Registered);
        event.setQuizAnswerSheet(quizAnswerSheet);
        return event;
    }

    public static QuizAnswerSheetEvent newQuizAnswerSheetModifiedEvent(String quizAnswerSheetId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizAnswerSheetEvent event = new QuizAnswerSheetEvent(CqrsDataEventType.Modified);
        event.setQuizAnswerSheetId(quizAnswerSheetId);
        event.setNameValues(nameValues);
        return event;
    }

    public static QuizAnswerSheetEvent newQuizAnswerSheetModifiedEvent(QuizAnswerSheet quizAnswerSheet) {
        /* Autogen by nara studio */
        QuizAnswerSheetEvent event = new QuizAnswerSheetEvent(CqrsDataEventType.Modified);
        event.setQuizAnswerSheetId(quizAnswerSheet.getId());
        event.setQuizAnswerSheet(quizAnswerSheet);
        return event;
    }

    public static QuizAnswerSheetEvent newQuizAnswerSheetRemovedEvent(QuizAnswerSheet quizAnswerSheet) {
        /* Autogen by nara studio */
        QuizAnswerSheetEvent event = new QuizAnswerSheetEvent(CqrsDataEventType.Removed);
        event.setQuizAnswerSheetId(quizAnswerSheet.getId());
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static QuizAnswerSheetEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, QuizAnswerSheetEvent.class);
    }
}
