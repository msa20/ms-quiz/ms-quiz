/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.rest;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.microservice.quiz.aggregate.quizsheet.store.QuizSheetStore;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizSheetJpo;
import javax.persistence.EntityManager;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizSheetQuery;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizSheetDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizSheetsDynamicQuery;

@RestController
@RequestMapping("/aggregate/quiz-sheet/query")
public class QuizSheetQueryResource implements QuizSheetQueryFacade {
    /* Autogen by nara studio */
    private final QuizSheetStore quizSheetStore;
    private final RdbQueryRequest<QuizSheetJpo> request;

    public QuizSheetQueryResource(QuizSheetStore quizSheetStore, EntityManager entityManager) {
        /* Autogen by nara studio */
        this.quizSheetStore = quizSheetStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QuizSheetQuery execute(@RequestBody QuizSheetQuery quizSheetQuery) {
        /* Autogen by nara studio */
        quizSheetQuery.execute(quizSheetStore);
        return quizSheetQuery;
    }

    @Override
    @PostMapping("/dynamic-single")
    public QuizSheetDynamicQuery execute(@RequestBody QuizSheetDynamicQuery quizSheetDynamicQuery) {
        /* Autogen by nara studio */
        quizSheetDynamicQuery.execute(request);
        return quizSheetDynamicQuery;
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QuizSheetsDynamicQuery execute(@RequestBody QuizSheetsDynamicQuery quizSheetsDynamicQuery) {
        /* Autogen by nara studio */
        quizSheetsDynamicQuery.execute(request);
        return quizSheetsDynamicQuery;
    }
}
