/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.rest;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.microservice.quiz.aggregate.quizsheet.store.QuizAnswerSheetStore;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizAnswerSheetJpo;
import javax.persistence.EntityManager;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerSheetQuery;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerSheetDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerSheetsDynamicQuery;

@RestController
@RequestMapping("/aggregate/quiz-answer-sheet/query")
public class QuizAnswerSheetQueryResource implements QuizAnswerSheetQueryFacade {
    /* Autogen by nara studio */
    private final QuizAnswerSheetStore quizAnswerSheetStore;
    private final RdbQueryRequest<QuizAnswerSheetJpo> request;

    public QuizAnswerSheetQueryResource(QuizAnswerSheetStore quizAnswerSheetStore, EntityManager entityManager) {
        /* Autogen by nara studio */
        this.quizAnswerSheetStore = quizAnswerSheetStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QuizAnswerSheetQuery execute(@RequestBody QuizAnswerSheetQuery quizAnswerSheetQuery) {
        /* Autogen by nara studio */
        quizAnswerSheetQuery.execute(quizAnswerSheetStore);
        return quizAnswerSheetQuery;
    }

    @Override
    @PostMapping("/dynamic-single")
    public QuizAnswerSheetDynamicQuery execute(@RequestBody QuizAnswerSheetDynamicQuery quizAnswerSheetDynamicQuery) {
        /* Autogen by nara studio */
        quizAnswerSheetDynamicQuery.execute(request);
        return quizAnswerSheetDynamicQuery;
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QuizAnswerSheetsDynamicQuery execute(@RequestBody QuizAnswerSheetsDynamicQuery quizAnswerSheetsDynamicQuery) {
        /* Autogen by nara studio */
        quizAnswerSheetsDynamicQuery.execute(request);
        return quizAnswerSheetsDynamicQuery;
    }
}
