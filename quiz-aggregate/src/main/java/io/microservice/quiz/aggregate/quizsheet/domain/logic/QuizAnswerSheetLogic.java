/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.domain.logic;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import io.microservice.quiz.aggregate.quizsheet.store.QuizAnswerSheetStore;
import io.microservice.quiz.aggregate.quizsheet.api.command.command.QuizAnswerSheetCommand;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import java.lang.String;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizAnswerSheetCdo;
import java.util.NoSuchElementException;
import io.microservice.quiz.aggregate.quizsheet.domain.event.QuizAnswerSheetEvent;

import java.util.List;
import java.util.stream.Collectors;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswerSheet;
import io.naradrama.prologue.domain.Offset;
import io.naradrama.prologue.domain.NameValueList;

@Service
@Transactional
public class QuizAnswerSheetLogic {
    /* Autogen by nara studio */
    private final QuizSheetLogic quizSheetLogic;
    private final QuizAnswerSheetStore quizAnswerSheetStore;
    private final ApplicationEventPublisher eventPublisher;

    public QuizAnswerSheetLogic(QuizSheetLogic quizSheetLogic, QuizAnswerSheetStore quizAnswerSheetStore, ApplicationEventPublisher eventPublisher) {
        /* Autogen by nara studio */
        this.quizSheetLogic = quizSheetLogic;
        this.quizAnswerSheetStore = quizAnswerSheetStore;
        this.eventPublisher = eventPublisher;
    }

    public QuizAnswerSheetCommand routeCommand(QuizAnswerSheetCommand command) {
        /* Autogen by nara studio */
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.getQuizAnswerSheetCdos().size() > 0) {
                    List<String> entityIds = this.registerQuizAnswerSheets(command.getQuizAnswerSheetCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = this.registerQuizAnswerSheet(command.getQuizAnswerSheetCdo());
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyQuizAnswerSheet(command.getQuizAnswerSheetId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getQuizAnswerSheetId()));
                break;
            case Remove:
                this.removeQuizAnswerSheet(command.getQuizAnswerSheetId());
                command.setCommandResponse(new CommandResponse(command.getQuizAnswerSheetId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerQuizAnswerSheet(QuizAnswerSheetCdo quizAnswerSheetCdo) {
        /* Autogen by nara studio */
        QuizAnswerSheet quizAnswerSheet = new QuizAnswerSheet(quizAnswerSheetCdo);
        if (quizAnswerSheetStore.exists(quizAnswerSheet.getId())) {
            throw new IllegalArgumentException("quizAnswerSheet already exists. " + quizAnswerSheet.getId());
        }
        quizAnswerSheetStore.create(quizAnswerSheet);
        QuizAnswerSheetEvent quizAnswerSheetEvent = QuizAnswerSheetEvent.newQuizAnswerSheetRegisteredEvent(quizAnswerSheet);
        eventPublisher.publishEvent(quizAnswerSheetEvent);
        return quizAnswerSheet.getId();
    }

    public List<String> registerQuizAnswerSheets(List<QuizAnswerSheetCdo> quizAnswerSheetCdos) {
        /* Autogen by nara studio */
        return quizAnswerSheetCdos.stream().map(quizAnswerSheetCdo -> this.registerQuizAnswerSheet(quizAnswerSheetCdo)).collect(Collectors.toList());
    }

    public QuizAnswerSheet findQuizAnswerSheet(String quizAnswerSheetId) {
        /* Autogen by nara studio */
        QuizAnswerSheet quizAnswerSheet = quizAnswerSheetStore.retrieve(quizAnswerSheetId);
        if (quizAnswerSheet == null) {
            throw new NoSuchElementException("QuizAnswerSheet id: " + quizAnswerSheetId);
        }
        return quizAnswerSheet;
    }

    public List<QuizAnswerSheet> findAllQuizAnswerSheet(Offset offset) {
        /* Autogen by nara studio */
        return quizAnswerSheetStore.retrieveAll(offset);
    }

    public void modifyQuizAnswerSheet(String quizAnswerSheetId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizAnswerSheet quizAnswerSheet = findQuizAnswerSheet(quizAnswerSheetId);
        quizAnswerSheet.modifyValues(nameValues);
        quizAnswerSheetStore.update(quizAnswerSheet);
        QuizAnswerSheetEvent quizAnswerSheetEvent = QuizAnswerSheetEvent.newQuizAnswerSheetModifiedEvent(quizAnswerSheetId, nameValues);
        eventPublisher.publishEvent(quizAnswerSheetEvent);
    }

    public void modifyQuizAnswerSheet(QuizAnswerSheet quizAnswerSheet) {
        // Check existence
        QuizAnswerSheet foundQuizAnswerSheet = findQuizAnswerSheet(quizAnswerSheet.getId());
        quizAnswerSheetStore.update(quizAnswerSheet);
        QuizAnswerSheetEvent quizAnswerSheetEvent = QuizAnswerSheetEvent.newQuizAnswerSheetModifiedEvent(quizAnswerSheet);
        eventPublisher.publishEvent(quizAnswerSheetEvent);
    }

    public void removeQuizAnswerSheet(String quizAnswerSheetId) {
        /* Autogen by nara studio */
        QuizAnswerSheet quizAnswerSheet = findQuizAnswerSheet(quizAnswerSheetId);
        quizAnswerSheetStore.delete(quizAnswerSheet);
        QuizAnswerSheetEvent quizAnswerSheetEvent = QuizAnswerSheetEvent.newQuizAnswerSheetRemovedEvent(quizAnswerSheet);
        eventPublisher.publishEvent(quizAnswerSheetEvent);
    }

    public boolean existsQuizAnswerSheet(String quizAnswerSheetId) {
        /* Autogen by nara studio */
        return quizAnswerSheetStore.exists(quizAnswerSheetId);
    }

    public void handleEventForProjection(QuizAnswerSheetEvent quizAnswerSheetEvent) {
        /* Autogen by nara studio */
        switch(quizAnswerSheetEvent.getCqrsDataEventType()) {
            case Registered:
                quizAnswerSheetStore.create(quizAnswerSheetEvent.getQuizAnswerSheet());
                break;
            case Modified:
                QuizAnswerSheet quizAnswerSheet = quizAnswerSheetStore.retrieve(quizAnswerSheetEvent.getQuizAnswerSheetId());
                quizAnswerSheet.modifyValues(quizAnswerSheetEvent.getNameValues());
                quizAnswerSheetStore.update(quizAnswerSheet);
                break;
            case Removed:
                quizAnswerSheetStore.delete(quizAnswerSheetEvent.getQuizAnswerSheetId());
                break;
        }
    }
}
