/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizSheet;
import io.naradrama.prologue.domain.Offset;

import java.util.List;

public interface QuizSheetStore {
    /* Autogen by nara studio */
    void create(QuizSheet quizSheet);
    QuizSheet retrieve(String id);
    List<QuizSheet> retrieveAll(Offset offset);
    void update(QuizSheet quizSheet);
    void delete(QuizSheet quizSheet);
    void delete(String id);
    boolean exists(String id);
}
