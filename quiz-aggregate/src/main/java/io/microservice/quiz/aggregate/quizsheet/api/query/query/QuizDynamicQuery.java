/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.query;

import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizJpo;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.Quiz;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;

@Getter
@Setter
@NoArgsConstructor
public class QuizDynamicQuery extends CqrsDynamicQuery<Quiz> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<QuizJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), QuizJpo.class);
        TypedQuery<QuizJpo> query = RdbQueryBuilder.build(request);
        QuizJpo quizJpo = query.getSingleResult();

        if (quizJpo != null) {
            setQueryResult(quizJpo.toDomain());
        } else {
            setQueryResult(null);
        }
    }
}
