/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswerSheet;
import java.lang.String;
import io.microservice.quiz.aggregate.quizsheet.store.QuizAnswerSheetStore;

@Getter
@Setter
@NoArgsConstructor
public class QuizAnswerSheetQuery extends CqrsBaseQuery<QuizAnswerSheet> {
    /* Autogen by nara studio */
    private String quizAnswerSheetId;

    public void execute(QuizAnswerSheetStore quizAnswerSheetStore) {
        /* Autogen by nara studio */
        setQueryResult(quizAnswerSheetStore.retrieve(quizAnswerSheetId));
    }
}
