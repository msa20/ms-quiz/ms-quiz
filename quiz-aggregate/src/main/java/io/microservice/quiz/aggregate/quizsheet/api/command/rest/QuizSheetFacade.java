/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.command.rest;


import io.microservice.quiz.aggregate.quizsheet.api.command.command.*;

public interface QuizSheetFacade {
    /* Autogen by nara studio */
    QuizCommand executeQuiz(QuizCommand quizCommand);
    QuizAnswerCommand executeQuizAnswer(QuizAnswerCommand quizAnswerCommand);
    QuizAnswerSheetCommand executeQuizAnswerSheet(QuizAnswerSheetCommand quizAnswerSheetCommand);
    QuizSheetCommand executeQuizSheet(QuizSheetCommand quizSheetCommand);
}
