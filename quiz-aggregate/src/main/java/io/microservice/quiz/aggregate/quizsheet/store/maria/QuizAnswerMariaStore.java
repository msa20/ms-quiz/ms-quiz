/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store.maria;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswer;
import io.microservice.quiz.aggregate.quizsheet.store.QuizAnswerStore;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizAnswerJpo;
import io.microservice.quiz.aggregate.quizsheet.store.maria.repository.QuizAnswerMariaRepository;
import io.naradrama.prologue.domain.Offset;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class QuizAnswerMariaStore implements QuizAnswerStore {
    /* Autogen by nara studio */
    private final QuizAnswerMariaRepository quizAnswerMariaRepository;

    public QuizAnswerMariaStore(QuizAnswerMariaRepository QuizAnswerMariaRepository) {
        /* Autogen by nara studio */
        this.quizAnswerMariaRepository = QuizAnswerMariaRepository;
    }

    @Override
    public void create(QuizAnswer quizAnswer) {
        /* Autogen by nara studio */
        QuizAnswerJpo quizAnswerJpo = new QuizAnswerJpo(quizAnswer);
        quizAnswerMariaRepository.save(quizAnswerJpo);
    }

    @Override
    public QuizAnswer retrieve(String id) {
        /* Autogen by nara studio */
        Optional<QuizAnswerJpo> quizAnswerJpo = quizAnswerMariaRepository.findById(id);
        return quizAnswerJpo.map(QuizAnswerJpo::toDomain).orElse(null);
    }

    @Override
    public List<QuizAnswer> retrieveAll(Offset offset) {
        /* Autogen by nara studio */
        Pageable pageable = createPageable(offset);
        Page<QuizAnswerJpo> page = quizAnswerMariaRepository.findAll(pageable);
        offset.setTotalCount(page.getTotalElements());
        List<QuizAnswerJpo> quizAnswerJpos = page.getContent();
        return QuizAnswerJpo.toDomains(quizAnswerJpos);
    }

    @Override
    public void update(QuizAnswer quizAnswer) {
        /* Autogen by nara studio */
        QuizAnswerJpo quizAnswerJpo = new QuizAnswerJpo(quizAnswer);
        quizAnswerMariaRepository.save(quizAnswerJpo);
    }

    @Override
    public void delete(QuizAnswer quizAnswer) {
        /* Autogen by nara studio */
        quizAnswerMariaRepository.deleteById(quizAnswer.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        quizAnswerMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return quizAnswerMariaRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
