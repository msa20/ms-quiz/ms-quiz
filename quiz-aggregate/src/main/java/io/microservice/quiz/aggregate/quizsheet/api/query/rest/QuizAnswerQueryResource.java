/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.rest;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.microservice.quiz.aggregate.quizsheet.store.QuizAnswerStore;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizAnswerJpo;
import javax.persistence.EntityManager;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerQuery;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswersDynamicQuery;

@RestController
@RequestMapping("/aggregate/quiz-answer/query")
public class QuizAnswerQueryResource implements QuizAnswerQueryFacade {
    /* Autogen by nara studio */
    private final QuizAnswerStore quizAnswerStore;
    private final RdbQueryRequest<QuizAnswerJpo> request;

    public QuizAnswerQueryResource(QuizAnswerStore quizAnswerStore, EntityManager entityManager) {
        /* Autogen by nara studio */
        this.quizAnswerStore = quizAnswerStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QuizAnswerQuery execute(@RequestBody QuizAnswerQuery quizAnswerQuery) {
        /* Autogen by nara studio */
        quizAnswerQuery.execute(quizAnswerStore);
        return quizAnswerQuery;
    }

    @Override
    @PostMapping("/dynamic-single")
    public QuizAnswerDynamicQuery execute(@RequestBody QuizAnswerDynamicQuery quizAnswerDynamicQuery) {
        /* Autogen by nara studio */
        quizAnswerDynamicQuery.execute(request);
        return quizAnswerDynamicQuery;
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QuizAnswersDynamicQuery execute(@RequestBody QuizAnswersDynamicQuery quizAnswersDynamicQuery) {
        /* Autogen by nara studio */
        quizAnswersDynamicQuery.execute(request);
        return quizAnswersDynamicQuery;
    }
}
