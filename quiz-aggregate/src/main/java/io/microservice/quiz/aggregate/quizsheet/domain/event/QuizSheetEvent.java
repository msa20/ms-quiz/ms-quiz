/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.domain.event;

import lombok.Getter;
import lombok.Setter;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizSheet;
import java.lang.String;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
public class QuizSheetEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private QuizSheet quizSheet;
    private String quizSheetId;
    private NameValueList nameValues;

    protected QuizSheetEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static QuizSheetEvent newQuizSheetRegisteredEvent(QuizSheet quizSheet) {
        /* Autogen by nara studio */
        QuizSheetEvent event = new QuizSheetEvent(CqrsDataEventType.Registered);
        event.setQuizSheet(quizSheet);
        return event;
    }

    public static QuizSheetEvent newQuizSheetModifiedEvent(String quizSheetId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizSheetEvent event = new QuizSheetEvent(CqrsDataEventType.Modified);
        event.setQuizSheetId(quizSheetId);
        event.setNameValues(nameValues);
        return event;
    }

    public static QuizSheetEvent newQuizSheetModifiedEvent(QuizSheet quizSheet) {
        /* Autogen by nara studio */
        QuizSheetEvent event = new QuizSheetEvent(CqrsDataEventType.Modified);
        event.setQuizSheetId(quizSheet.getId());
        event.setQuizSheet(quizSheet);
        return event;
    }

    public static QuizSheetEvent newQuizSheetRemovedEvent(String quizSheetId) {
        /* Autogen by nara studio */
        QuizSheetEvent event = new QuizSheetEvent(CqrsDataEventType.Removed);
        event.setQuizSheetId(quizSheetId);
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static QuizSheetEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, QuizSheetEvent.class);
    }
}
