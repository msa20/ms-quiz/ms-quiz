/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.rest;

import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerSheetQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerSheetDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerSheetsDynamicQuery;

public interface QuizAnswerSheetQueryFacade {
    /* Autogen by nara studio */
    QuizAnswerSheetQuery execute(QuizAnswerSheetQuery quizAnswerSheetQuery);
    QuizAnswerSheetDynamicQuery execute(QuizAnswerSheetDynamicQuery quizAnswerSheetDynamicQuery);
    QuizAnswerSheetsDynamicQuery execute(QuizAnswerSheetsDynamicQuery quizAnswerSheetsDynamicQuery);
}
