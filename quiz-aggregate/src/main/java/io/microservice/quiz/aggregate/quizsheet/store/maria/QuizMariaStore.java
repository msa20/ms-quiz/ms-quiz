/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store.maria;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.Quiz;
import io.microservice.quiz.aggregate.quizsheet.store.QuizStore;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizJpo;
import io.microservice.quiz.aggregate.quizsheet.store.maria.repository.QuizMariaRepository;
import io.naradrama.prologue.domain.Offset;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class QuizMariaStore implements QuizStore {
    /* Autogen by nara studio */
    private final QuizMariaRepository quizMariaRepository;

    public QuizMariaStore(QuizMariaRepository QuizMariaRepository) {
        /* Autogen by nara studio */
        this.quizMariaRepository = QuizMariaRepository;
    }

    @Override
    public void create(Quiz quiz) {
        /* Autogen by nara studio */
        QuizJpo quizJpo = new QuizJpo(quiz);
        quizMariaRepository.save(quizJpo);
    }

    @Override
    public Quiz retrieve(String id) {
        /* Autogen by nara studio */
        Optional<QuizJpo> quizJpo = quizMariaRepository.findById(id);
        return quizJpo.map(QuizJpo::toDomain).orElse(null);
    }

    @Override
    public List<Quiz> retrieveAll(Offset offset) {
        /* Autogen by nara studio */
        Pageable pageable = createPageable(offset);
        Page<QuizJpo> page = quizMariaRepository.findAll(pageable);
        offset.setTotalCount(page.getTotalElements());
        List<QuizJpo> quizJpos = page.getContent();
        return QuizJpo.toDomains(quizJpos);
    }

    @Override
    public void update(Quiz quiz) {
        /* Autogen by nara studio */
        QuizJpo quizJpo = new QuizJpo(quiz);
        quizMariaRepository.save(quizJpo);
    }

    @Override
    public void delete(Quiz quiz) {
        /* Autogen by nara studio */
        quizMariaRepository.deleteById(quiz.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        quizMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return quizMariaRepository.existsById(id);
    }

    @Override
    public List<Quiz> retrieveByQuizSheetId(String quizSheetId) {
        //
        List<QuizJpo> quizzes = quizMariaRepository.findByQuizSheetId(quizSheetId);
        return QuizJpo.toDomains(quizzes);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
