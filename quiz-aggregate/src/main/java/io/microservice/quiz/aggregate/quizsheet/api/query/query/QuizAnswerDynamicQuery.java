/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswer;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizAnswerJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;

@Getter
@Setter
@NoArgsConstructor
public class QuizAnswerDynamicQuery extends CqrsDynamicQuery<QuizAnswer> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<QuizAnswerJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), QuizAnswerJpo.class);
        TypedQuery<QuizAnswerJpo> query = RdbQueryBuilder.build(request);
        QuizAnswerJpo quizAnswerJpo = query.getSingleResult();

        if (quizAnswerJpo != null) {
            setQueryResult(quizAnswerJpo.toDomain());
        } else {
            setQueryResult(null);
        }
    }
}
