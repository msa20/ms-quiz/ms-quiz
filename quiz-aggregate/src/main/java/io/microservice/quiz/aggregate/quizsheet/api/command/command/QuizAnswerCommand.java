/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizAnswerCdo;
import java.util.List;
import java.lang.String;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class QuizAnswerCommand extends CqrsBaseCommand {
    /* Autogen by nara studio */
    private QuizAnswerCdo quizAnswerCdo;
    private List<QuizAnswerCdo> quizAnswerCdos;
    private String quizAnswerId;
    private NameValueList nameValues;

    protected QuizAnswerCommand(CqrsBaseCommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static QuizAnswerCommand newRegisterQuizAnswerCommand(QuizAnswerCdo quizAnswerCdo) {
        /* Autogen by nara studio */
        QuizAnswerCommand command = new QuizAnswerCommand(CqrsBaseCommandType.Register);
        command.setQuizAnswerCdo(quizAnswerCdo);
        return command;
    }

    public static QuizAnswerCommand newRegisterQuizAnswerCommand(List<QuizAnswerCdo> quizAnswerCdos) {
        /* Autogen by nara studio */
        QuizAnswerCommand command = new QuizAnswerCommand(CqrsBaseCommandType.Register);
        command.setQuizAnswerCdos(quizAnswerCdos);
        return command;
    }

    public static QuizAnswerCommand newModifyQuizAnswerCommand(String quizAnswerId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizAnswerCommand command = new QuizAnswerCommand(CqrsBaseCommandType.Modify);
        command.setQuizAnswerId(quizAnswerId);
        command.setNameValues(nameValues);
        return command;
    }

    public static QuizAnswerCommand newRemoveQuizAnswerCommand(String quizAnswerId) {
        /* Autogen by nara studio */
        QuizAnswerCommand command = new QuizAnswerCommand(CqrsBaseCommandType.Remove);
        command.setQuizAnswerId(quizAnswerId);
        return command;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static QuizAnswerCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, QuizAnswerCommand.class);
    }
}
