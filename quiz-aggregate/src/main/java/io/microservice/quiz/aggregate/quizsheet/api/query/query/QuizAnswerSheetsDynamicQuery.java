/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import java.util.List;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswerSheet;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizAnswerSheetJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import io.naradrama.prologue.domain.Offset;
import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
public class QuizAnswerSheetsDynamicQuery extends CqrsDynamicQuery<List<QuizAnswerSheet>> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<QuizAnswerSheetJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), QuizAnswerSheetJpo.class);
        Offset offset = getOffset();
        TypedQuery<QuizAnswerSheetJpo> query = RdbQueryBuilder.build(request, offset);
        query.setFirstResult(offset.getOffset());
        query.setMaxResults(offset.getLimit());
        List<QuizAnswerSheetJpo> quizAnswerSheetJpos = query.getResultList();
        setQueryResult(QuizAnswerSheetJpo.toDomains(quizAnswerSheetJpos));
        if (nonNull(getOffset()) && getOffset().isTotalCountRequested()) {
            TypedQuery<Long> countQuery = RdbQueryBuilder.buildForCount(request);
            long totalCount = countQuery.getSingleResult();
            Offset countableOffset = getOffset();
            countableOffset.setTotalCount((int) totalCount);
            setOffset(countableOffset);
        }
    }
}
