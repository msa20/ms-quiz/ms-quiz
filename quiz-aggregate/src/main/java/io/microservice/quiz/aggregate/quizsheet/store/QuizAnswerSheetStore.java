/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store;

import io.naradrama.prologue.domain.Offset;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswerSheet;

import java.util.List;

public interface QuizAnswerSheetStore {
    /* Autogen by nara studio */
    void create(QuizAnswerSheet quizAnswerSheet);
    QuizAnswerSheet retrieve(String id);
    List<QuizAnswerSheet> retrieveAll(Offset offset);
    void update(QuizAnswerSheet quizAnswerSheet);
    void delete(QuizAnswerSheet quizAnswerSheet);
    void delete(String id);
    boolean exists(String id);
}
