/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.domain.logic;

import io.microservice.quiz.aggregate.quizsheet.api.command.command.QuizCommand;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizCdo;
import io.microservice.quiz.aggregate.quizsheet.store.QuizStore;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import java.lang.String;
import java.util.NoSuchElementException;
import io.microservice.quiz.aggregate.quizsheet.domain.event.QuizEvent;

import java.util.List;
import java.util.stream.Collectors;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.Quiz;
import io.naradrama.prologue.domain.Offset;
import io.naradrama.prologue.domain.NameValueList;

@Service
@Transactional
public class QuizLogic {
    /* Autogen by nara studio */
    private final QuizSheetLogic quizSheetLogic;
    private final QuizStore quizStore;
    private final ApplicationEventPublisher eventPublisher;

    public QuizLogic(QuizSheetLogic quizSheetLogic, QuizStore quizStore, ApplicationEventPublisher eventPublisher) {
        /* Autogen by nara studio */
        this.quizSheetLogic = quizSheetLogic;
        this.quizStore = quizStore;
        this.eventPublisher = eventPublisher;
    }

    public QuizCommand routeCommand(QuizCommand command) {
        /* Autogen by nara studio */
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.getQuizCdos().size() > 0) {
                    List<String> entityIds = this.registerQuizs(command.getQuizCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = this.registerQuiz(command.getQuizCdo());
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyQuiz(command.getQuizId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getQuizId()));
                break;
            case Remove:
                this.removeQuiz(command.getQuizId());
                command.setCommandResponse(new CommandResponse(command.getQuizId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerQuiz(QuizCdo quizCdo) {
        /* Autogen by nara studio */
        Quiz quiz = new Quiz(quizCdo);
        if (quizStore.exists(quiz.getId())) {
            throw new IllegalArgumentException("quiz already exists. " + quiz.getId());
        }
        quizStore.create(quiz);
        QuizEvent quizEvent = QuizEvent.newQuizRegisteredEvent(quiz);
        eventPublisher.publishEvent(quizEvent);
        return quiz.getId();
    }

    public List<String> registerQuizs(List<QuizCdo> quizCdos) {
        /* Autogen by nara studio */
        return quizCdos.stream().map(this::registerQuiz).collect(Collectors.toList());
    }

    public Quiz findQuiz(String quizId) {
        /* Autogen by nara studio */
        Quiz quiz = quizStore.retrieve(quizId);
        if (quiz == null) {
            throw new NoSuchElementException("Quiz id: " + quizId);
        }
        return quiz;
    }

    public List<Quiz> findAllQuiz(Offset offset) {
        /* Autogen by nara studio */
        return quizStore.retrieveAll(offset);
    }

    public List<Quiz> findQuizByQuizSheetId(String quizSheetId) {
        //
        return quizStore.retrieveByQuizSheetId(quizSheetId);
    }

    public void modifyQuiz(String quizId, NameValueList nameValues) {
        /* Autogen by nara studio */
        Quiz quiz = findQuiz(quizId);
        quiz.modifyValues(nameValues);
        quizStore.update(quiz);
        QuizEvent quizEvent = QuizEvent.newQuizModifiedEvent(quizId, nameValues);
        eventPublisher.publishEvent(quizEvent);
    }

    public void modifyQuiz(Quiz quiz) {
        // Check existence
        Quiz foundQuiz = findQuiz(quiz.getId());
        quizStore.update(quiz);
        QuizEvent quizEvent = QuizEvent.newQuizModifiedEvent(quiz);
        eventPublisher.publishEvent(quizEvent);
    }

    public void removeQuiz(String quizId) {
        /* Autogen by nara studio */
        Quiz quiz = findQuiz(quizId);
        quizStore.delete(quiz);
        QuizEvent quizEvent = QuizEvent.newQuizRemovedEvent(quiz);
        eventPublisher.publishEvent(quizEvent);
    }

    public boolean existsQuiz(String quizId) {
        /* Autogen by nara studio */
        return quizStore.exists(quizId);
    }

    public void handleEventForProjection(QuizEvent quizEvent) {
        /* Autogen by nara studio */
        switch(quizEvent.getCqrsDataEventType()) {
            case Registered:
                quizStore.create(quizEvent.getQuiz());
                break;
            case Modified:
                Quiz quiz = quizStore.retrieve(quizEvent.getQuizId());
                quiz.modifyValues(quizEvent.getNameValues());
                quizStore.update(quiz);
                break;
            case Removed:
                quizStore.delete(quizEvent.getQuizId());
                break;
        }
    }
}
