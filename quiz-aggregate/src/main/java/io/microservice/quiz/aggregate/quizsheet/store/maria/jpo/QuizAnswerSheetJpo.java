/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store.maria.jpo;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.store.jpa.DomainEntityJpo;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswerSheet;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "QUIZ_ANSWER_SHEET")
public class QuizAnswerSheetJpo extends DomainEntityJpo {
    /* Autogen by nara studio */
    private String groupId;
    private String quizSheetId;
    private String writerId;
    private String writerName;
    private String subject;
    private Boolean passed; // io.naradrama.exam.aggregate.quiz.domain.entity.Boolean

    public QuizAnswerSheetJpo(QuizAnswerSheet quizAnswerSheet) {
        /* Autogen by nara studio */
        super(quizAnswerSheet);
        BeanUtils.copyProperties(quizAnswerSheet, this);

        this.writerId = quizAnswerSheet.getWriter().getId();
        this.writerName = quizAnswerSheet.getWriter().getName();
    }

    public QuizAnswerSheet toDomain() {
        /* Autogen by nara studio */
        QuizAnswerSheet quizAnswerSheet = new QuizAnswerSheet(getId());
        BeanUtils.copyProperties(this, quizAnswerSheet);

        quizAnswerSheet.setWriter(IdName.of(this.writerId, this.writerName));
        return quizAnswerSheet;
    }

    public static List<QuizAnswerSheet> toDomains(List<QuizAnswerSheetJpo> quizAnswerSheetJpos) {
        /* Autogen by nara studio */
        return quizAnswerSheetJpos.stream().map(QuizAnswerSheetJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
