/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store;

import io.naradrama.prologue.domain.Offset;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswer;

import java.util.List;

public interface QuizAnswerStore {
    /* Autogen by nara studio */
    void create(QuizAnswer quizAnswer);
    QuizAnswer retrieve(String id);
    List<QuizAnswer> retrieveAll(Offset offset);
    void update(QuizAnswer quizAnswer);
    void delete(QuizAnswer quizAnswer);
    void delete(String id);
    boolean exists(String id);
}
