/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store.maria.jpo;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.store.jpa.DomainEntityJpo;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizSheet;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizSheetCategory;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizSheetState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "QUIZ_SHEET")
public class QuizSheetJpo extends DomainEntityJpo {
    /* Autogen by nara studio */
    private String groupId;
    private String title;
    private String subject;
    private String writerId;
    private String writerName;
    @Enumerated(EnumType.STRING)
    private QuizSheetCategory quizSheetCategory;
    @Enumerated(EnumType.STRING)
    private QuizSheetState quizSheetState; // io.naradrama.exam.aggregate.quiz.domain.entity.vo.QuizSheetState

    public QuizSheetJpo(QuizSheet quizSheet) {
        /* Autogen by nara studio */
        super(quizSheet);
        BeanUtils.copyProperties(quizSheet, this);

        this.writerId = quizSheet.getWriter().getId();
        this.writerName = quizSheet.getWriter().getName();
    }

    public QuizSheet toDomain() {
        /* Autogen by nara studio */
        QuizSheet quizSheet = new QuizSheet(getId());
        BeanUtils.copyProperties(this, quizSheet);
        quizSheet.setWriter(IdName.of(this.writerId, this.writerName));
        return quizSheet;
    }

    public static List<QuizSheet> toDomains(List<QuizSheetJpo> quizSheetJpos) {
        /* Autogen by nara studio */
        return quizSheetJpos.stream().map(QuizSheetJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
