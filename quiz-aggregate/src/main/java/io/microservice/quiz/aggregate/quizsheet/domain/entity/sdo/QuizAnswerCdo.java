package io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizGradingResult;
import io.naradrama.prologue.domain.IdName;

import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class QuizAnswerCdo implements JsonSerializable {
    //
    private String quizId;
    private String quizAnswerSheetId;

    private QuizGradingResult quizGradingResult;

    private IdName writer;

    public String toString() {
        //
        return toJson();
    }

    public static QuizAnswerCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, QuizAnswerCdo.class);
    }
}
