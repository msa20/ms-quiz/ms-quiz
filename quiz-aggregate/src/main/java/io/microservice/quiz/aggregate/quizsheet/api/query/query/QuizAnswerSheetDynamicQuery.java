/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswerSheet;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizAnswerSheetJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;

@Getter
@Setter
@NoArgsConstructor
public class QuizAnswerSheetDynamicQuery extends CqrsDynamicQuery<QuizAnswerSheet> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<QuizAnswerSheetJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), QuizAnswerSheetJpo.class);
        TypedQuery<QuizAnswerSheetJpo> query = RdbQueryBuilder.build(request);
        QuizAnswerSheetJpo quizAnswerSheetJpo = query.getSingleResult();

        if (quizAnswerSheetJpo != null) {
            setQueryResult(quizAnswerSheetJpo.toDomain());
        } else {
            setQueryResult(null);
        }
    }
}
