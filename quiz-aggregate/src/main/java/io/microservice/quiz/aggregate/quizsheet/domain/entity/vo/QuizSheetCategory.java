package io.microservice.quiz.aggregate.quizsheet.domain.entity.vo;

public enum QuizSheetCategory {
    JavaScript,
    TypeScript,
    React,
    Mobx,
    Java,
    UML,
    SpringBoot,
    MicroService,
    Etc,
}
