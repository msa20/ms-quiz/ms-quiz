/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store.maria.repository;

import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizSheetJpo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface QuizSheetMariaRepository extends PagingAndSortingRepository<QuizSheetJpo, String> {
    /* Autogen by nara studio */
    Page<QuizSheetJpo> findAll(Pageable pageable);
}
