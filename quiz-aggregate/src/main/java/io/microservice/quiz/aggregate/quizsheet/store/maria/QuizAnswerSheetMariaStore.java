/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store.maria;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswerSheet;
import io.microservice.quiz.aggregate.quizsheet.store.QuizAnswerSheetStore;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizAnswerSheetJpo;
import io.microservice.quiz.aggregate.quizsheet.store.maria.repository.QuizAnswerSheetMariaRepository;
import io.naradrama.prologue.domain.Offset;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class QuizAnswerSheetMariaStore implements QuizAnswerSheetStore {
    /* Autogen by nara studio */
    private final QuizAnswerSheetMariaRepository quizAnswerSheetMariaRepository;

    public QuizAnswerSheetMariaStore(QuizAnswerSheetMariaRepository quizAnswerSheetMariaRepository) {
        /* Autogen by nara studio */
        this.quizAnswerSheetMariaRepository = quizAnswerSheetMariaRepository;
    }

    @Override
    public void create(QuizAnswerSheet quizAnswerSheet) {
        /* Autogen by nara studio */
        QuizAnswerSheetJpo quizAnswerSheetJpo = new QuizAnswerSheetJpo(quizAnswerSheet);
        quizAnswerSheetMariaRepository.save(quizAnswerSheetJpo);
    }

    @Override
    public QuizAnswerSheet retrieve(String id) {
        /* Autogen by nara studio */
        Optional<QuizAnswerSheetJpo> quizAnswerSheetJpo = quizAnswerSheetMariaRepository.findById(id);
        return quizAnswerSheetJpo.map(QuizAnswerSheetJpo::toDomain).orElse(null);
    }

    @Override
    public List<QuizAnswerSheet> retrieveAll(Offset offset) {
        /* Autogen by nara studio */
        Pageable pageable = createPageable(offset);
        Page<QuizAnswerSheetJpo> page = quizAnswerSheetMariaRepository.findAll(pageable);
        offset.setTotalCount(page.getTotalElements());
        List<QuizAnswerSheetJpo> quizAnswerSheetJpos = page.getContent();
        return QuizAnswerSheetJpo.toDomains(quizAnswerSheetJpos);
    }

    @Override
    public void update(QuizAnswerSheet quizAnswerSheet) {
        /* Autogen by nara studio */
        QuizAnswerSheetJpo quizAnswerSheetJpo = new QuizAnswerSheetJpo(quizAnswerSheet);
        quizAnswerSheetMariaRepository.save(quizAnswerSheetJpo);
    }

    @Override
    public void delete(QuizAnswerSheet quizAnswerSheet) {
        /* Autogen by nara studio */
        quizAnswerSheetMariaRepository.deleteById(quizAnswerSheet.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        quizAnswerSheetMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return quizAnswerSheetMariaRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
