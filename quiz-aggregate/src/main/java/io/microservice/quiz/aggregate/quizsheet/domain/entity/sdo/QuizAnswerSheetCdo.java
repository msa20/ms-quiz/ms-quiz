package io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo;

import io.naradrama.prologue.domain.IdName;

import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class QuizAnswerSheetCdo implements JsonSerializable {
    //
    private String groupId;

    private String quizSheetId;

    private IdName writer;
    private String subject;

    public String toString() {
        //
        return toJson();
    }

    public static QuizAnswerSheetCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, QuizAnswerSheetCdo.class);
    }
}
