package io.microservice.quiz.aggregate.quizsheet.domain.entity.vo;

public enum QuizDifficultyLevel {
    High,
    Average,
    Low
}
