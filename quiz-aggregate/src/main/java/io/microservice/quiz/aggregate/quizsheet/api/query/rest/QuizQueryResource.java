/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.rest;

import io.microservice.quiz.aggregate.quizsheet.store.QuizStore;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizJpo;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import javax.persistence.EntityManager;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizQuery;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizsDynamicQuery;

@RestController
@RequestMapping("/aggregate/quiz/query")
public class QuizQueryResource implements QuizQueryFacade {
    /* Autogen by nara studio */
    private final QuizStore quizStore;
    private final RdbQueryRequest<QuizJpo> request;

    public QuizQueryResource(QuizStore quizStore, EntityManager entityManager) {
        /* Autogen by nara studio */
        this.quizStore = quizStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QuizQuery execute(@RequestBody QuizQuery quizQuery) {
        /* Autogen by nara studio */
        quizQuery.execute(quizStore);
        return quizQuery;
    }

    @Override
    @PostMapping("/dynamic-single")
    public QuizDynamicQuery execute(@RequestBody QuizDynamicQuery quizDynamicQuery) {
        /* Autogen by nara studio */
        quizDynamicQuery.execute(request);
        return quizDynamicQuery;
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QuizsDynamicQuery execute(@RequestBody QuizsDynamicQuery quizsDynamicQuery) {
        /* Autogen by nara studio */
        quizsDynamicQuery.execute(request);
        return quizsDynamicQuery;
    }
}
