/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store.maria;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizSheet;
import io.microservice.quiz.aggregate.quizsheet.store.QuizSheetStore;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizSheetJpo;
import io.microservice.quiz.aggregate.quizsheet.store.maria.repository.QuizSheetMariaRepository;
import io.naradrama.prologue.domain.Offset;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class QuizSheetMariaStore implements QuizSheetStore {
    /* Autogen by nara studio */
    private final QuizSheetMariaRepository quizSheetMariaRepository;

    public QuizSheetMariaStore(QuizSheetMariaRepository QuizSheetMariaRepository) {
        /* Autogen by nara studio */
        this.quizSheetMariaRepository = QuizSheetMariaRepository;
    }

    @Override
    public void create(QuizSheet quizSheet) {
        /* Autogen by nara studio */
        QuizSheetJpo quizSheetJpo = new QuizSheetJpo(quizSheet);
        quizSheetMariaRepository.save(quizSheetJpo);
    }

    @Override
    public QuizSheet retrieve(String id) {
        /* Autogen by nara studio */
        Optional<QuizSheetJpo> quizSheetJpo = quizSheetMariaRepository.findById(id);
        return quizSheetJpo.map(QuizSheetJpo::toDomain).orElse(null);
    }

    @Override
    public List<QuizSheet> retrieveAll(Offset offset) {
        /* Autogen by nara studio */
        Pageable pageable = createPageable(offset);
        Page<QuizSheetJpo> page = quizSheetMariaRepository.findAll(pageable);
        offset.setTotalCount(page.getTotalElements());
        List<QuizSheetJpo> quizSheetJpos = page.getContent();
        return QuizSheetJpo.toDomains(quizSheetJpos);
    }

    @Override
    public void update(QuizSheet quizSheet) {
        /* Autogen by nara studio */
        QuizSheetJpo quizSheetJpo = new QuizSheetJpo(quizSheet);
        quizSheetMariaRepository.save(quizSheetJpo);
    }

    @Override
    public void delete(QuizSheet quizSheet) {
        /* Autogen by nara studio */
        quizSheetMariaRepository.deleteById(quizSheet.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        quizSheetMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return quizSheetMariaRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
