package io.microservice.quiz.aggregate.quizsheet.domain.entity.vo;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class QuizAnswerNameValueList implements JsonSerializable {
    //
    private String quizAnswerId;
    private NameValueList nameValueList;

    @Override
    public String toString() {
        return toJson();
    }

    public static QuizAnswerNameValueList fromJson(String json) {
        //
        return JsonUtil.fromJson(json, QuizAnswerNameValueList.class);
    }
}
