/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswer;
import java.lang.String;
import io.microservice.quiz.aggregate.quizsheet.store.QuizAnswerStore;

@Getter
@Setter
@NoArgsConstructor
public class QuizAnswerQuery extends CqrsBaseQuery<QuizAnswer> {
    /* Autogen by nara studio */
    private String quizAnswerId;

    public void execute(QuizAnswerStore quizAnswerStore) {
        /* Autogen by nara studio */
        setQueryResult(quizAnswerStore.retrieve(quizAnswerId));
    }
}
