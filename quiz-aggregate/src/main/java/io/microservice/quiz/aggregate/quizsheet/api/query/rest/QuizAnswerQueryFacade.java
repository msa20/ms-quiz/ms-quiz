/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.rest;

import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswerDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizAnswersDynamicQuery;

public interface QuizAnswerQueryFacade {
    /* Autogen by nara studio */
    QuizAnswerQuery execute(QuizAnswerQuery quizAnswerQuery);
    QuizAnswerDynamicQuery execute(QuizAnswerDynamicQuery quizAnswerDynamicQuery);
    QuizAnswersDynamicQuery execute(QuizAnswersDynamicQuery quizAnswersDynamicQuery);
}
