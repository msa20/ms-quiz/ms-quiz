/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizAnswerSheetCdo;
import java.util.List;
import java.lang.String;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class QuizAnswerSheetCommand extends CqrsBaseCommand {
    /* Autogen by nara studio */
    private QuizAnswerSheetCdo quizAnswerSheetCdo;
    private List<QuizAnswerSheetCdo> quizAnswerSheetCdos;
    private String quizAnswerSheetId;
    private NameValueList nameValues;

    protected QuizAnswerSheetCommand(CqrsBaseCommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static QuizAnswerSheetCommand newRegisterQuizAnswerSheetCommand(QuizAnswerSheetCdo quizAnswerSheetCdo) {
        /* Autogen by nara studio */
        QuizAnswerSheetCommand command = new QuizAnswerSheetCommand(CqrsBaseCommandType.Register);
        command.setQuizAnswerSheetCdo(quizAnswerSheetCdo);
        return command;
    }

    public static QuizAnswerSheetCommand newRegisterQuizAnswerSheetCommand(List<QuizAnswerSheetCdo> quizAnswerSheetCdos) {
        /* Autogen by nara studio */
        QuizAnswerSheetCommand command = new QuizAnswerSheetCommand(CqrsBaseCommandType.Register);
        command.setQuizAnswerSheetCdos(quizAnswerSheetCdos);
        return command;
    }

    public static QuizAnswerSheetCommand newModifyQuizAnswerSheetCommand(String quizAnswerSheetId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizAnswerSheetCommand command = new QuizAnswerSheetCommand(CqrsBaseCommandType.Modify);
        command.setQuizAnswerSheetId(quizAnswerSheetId);
        command.setNameValues(nameValues);
        return command;
    }

    public static QuizAnswerSheetCommand newRemoveQuizAnswerSheetCommand(String quizAnswerSheetId) {
        /* Autogen by nara studio */
        QuizAnswerSheetCommand command = new QuizAnswerSheetCommand(CqrsBaseCommandType.Remove);
        command.setQuizAnswerSheetId(quizAnswerSheetId);
        return command;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static QuizAnswerSheetCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, QuizAnswerSheetCommand.class);
    }
}
