/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.query;

import io.microservice.quiz.aggregate.quizsheet.store.QuizStore;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.Quiz;
import java.lang.String;

@Getter
@Setter
@NoArgsConstructor
public class QuizQuery extends CqrsBaseQuery<Quiz> {
    /* Autogen by nara studio */
    private String quizId;

    public void execute(QuizStore quizStore) {
        /* Autogen by nara studio */
        setQueryResult(quizStore.retrieve(quizId));
    }
}
