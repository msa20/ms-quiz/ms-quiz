package io.microservice.quiz.aggregate.quizsheet.domain.entity.vo;

public enum QuizGradingResult {
    //
    Correct,            // O
    InCorrect,          // X
}
