/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.rest;

import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizsDynamicQuery;

public interface QuizQueryFacade {
    /* Autogen by nara studio */
    QuizQuery execute(QuizQuery quizQuery);
    QuizDynamicQuery execute(QuizDynamicQuery quizDynamicQuery);
    QuizsDynamicQuery execute(QuizsDynamicQuery quizsDynamicQuery);
}
