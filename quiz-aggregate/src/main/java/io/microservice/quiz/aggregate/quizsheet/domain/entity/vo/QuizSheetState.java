package io.microservice.quiz.aggregate.quizsheet.domain.entity.vo;

public enum QuizSheetState {
    //
    Working,
    Published
}
