/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.rest;

import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizSheetQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizSheetDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.api.query.query.QuizSheetsDynamicQuery;

public interface QuizSheetQueryFacade {
    /* Autogen by nara studio */
    QuizSheetQuery execute(QuizSheetQuery quizSheetQuery);
    QuizSheetDynamicQuery execute(QuizSheetDynamicQuery quizSheetDynamicQuery);
    QuizSheetsDynamicQuery execute(QuizSheetsDynamicQuery quizSheetsDynamicQuery);
}
