/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store;

import io.naradrama.prologue.domain.Offset;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.Quiz;

import java.util.List;

public interface QuizStore {
    /* Autogen by nara studio */
    void create(Quiz quiz);
    Quiz retrieve(String id);
    List<Quiz> retrieveAll(Offset offset);
    void update(Quiz quiz);
    void delete(Quiz quiz);
    void delete(String id);
    boolean exists(String id);

    List<Quiz> retrieveByQuizSheetId(String quizSheetId);
}
