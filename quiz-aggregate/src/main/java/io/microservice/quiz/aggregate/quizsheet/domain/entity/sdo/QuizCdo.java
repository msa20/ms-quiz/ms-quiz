package io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizItem;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizCategory;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizDifficultyLevel;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizState;
import io.naradrama.prologue.domain.IdName;

import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class QuizCdo implements JsonSerializable {
    //
    private String groupId;
    private String quizSheetId;

    private IdName writer;
    private String no;
    private String text;
    private String subText;
    private QuizCategory quizCategory;
    private QuizDifficultyLevel quizDifficultyLevel;
    private QuizState quizState;
    private String correctSeq;
    private List<QuizItem> quizItems;

    public String toString() {
        //
        return toJson();
    }

    public static QuizCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, QuizCdo.class);
    }
}
