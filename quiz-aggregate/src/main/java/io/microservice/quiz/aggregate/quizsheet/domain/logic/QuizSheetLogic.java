/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.domain.logic;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import io.microservice.quiz.aggregate.quizsheet.store.QuizSheetStore;
import io.microservice.quiz.aggregate.quizsheet.api.command.command.QuizSheetCommand;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import java.lang.String;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizSheetCdo;
import io.microservice.quiz.aggregate.quizsheet.domain.event.QuizSheetEvent;

import java.util.List;
import java.util.stream.Collectors;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizSheet;
import java.util.NoSuchElementException;
import io.naradrama.prologue.domain.Offset;
import io.naradrama.prologue.domain.NameValueList;

@Service
@Transactional
public class QuizSheetLogic {
    /* Autogen by nara studio */
    private final QuizSheetStore quizSheetStore;
    private final ApplicationEventPublisher eventPublisher;

    public QuizSheetLogic(QuizSheetStore quizSheetStore, ApplicationEventPublisher eventPublisher) {
        /* Autogen by nara studio */
        this.quizSheetStore = quizSheetStore;
        this.eventPublisher = eventPublisher;
    }

    public QuizSheetCommand routeCommand(QuizSheetCommand command) {
        /* Autogen by nara studio */
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.getQuizSheetCdos().size() > 0) {
                    List<String> entityIds = this.registerQuizSheets(command.getQuizSheetCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = this.registerQuizSheet(command.getQuizSheetCdo());
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyQuizSheet(command.getQuizSheetId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getQuizSheetId()));
                break;
            case Remove:
                this.removeQuizSheet(command.getQuizSheetId());
                command.setCommandResponse(new CommandResponse(command.getQuizSheetId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerQuizSheet(QuizSheetCdo quizSheetCdo) {
        /* Autogen by nara studio */
        QuizSheet quizSheet = new QuizSheet(quizSheetCdo);
        if (quizSheetStore.exists(quizSheet.getId())) {
            throw new IllegalArgumentException("quizSheet already exists. " + quizSheet.getId());
        }
        quizSheetStore.create(quizSheet);
        QuizSheetEvent quizSheetEvent = QuizSheetEvent.newQuizSheetRegisteredEvent(quizSheet);
        eventPublisher.publishEvent(quizSheetEvent);
        return quizSheet.getId();
    }

    public List<String> registerQuizSheets(List<QuizSheetCdo> quizSheetCdos) {
        /* Autogen by nara studio */
        return quizSheetCdos.stream().map(quizSheetCdo -> this.registerQuizSheet(quizSheetCdo)).collect(Collectors.toList());
    }

    public QuizSheet findQuizSheet(String quizSheetId) {
        /* Autogen by nara studio */
        QuizSheet quizSheet = quizSheetStore.retrieve(quizSheetId);
        if (quizSheet == null) {
            throw new NoSuchElementException("QuizSheet id: " + quizSheetId);
        }
        return quizSheet;
    }

    public List<QuizSheet> findAllQuizSheet(Offset offset) {
        /* Autogen by nara studio */
        return quizSheetStore.retrieveAll(offset);
    }

    public void modifyQuizSheet(String quizSheetId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizSheet quizSheet = findQuizSheet(quizSheetId);
        quizSheet.modifyValues(nameValues);
        quizSheetStore.update(quizSheet);
        QuizSheetEvent quizSheetEvent = QuizSheetEvent.newQuizSheetModifiedEvent(quizSheetId, nameValues);
        eventPublisher.publishEvent(quizSheetEvent);
    }

    public void modifyQuizSheet(QuizSheet quizSheet) {
        // Check existence
        QuizSheet foundQuizSheet = findQuizSheet(quizSheet.getId());
        quizSheetStore.update(quizSheet);
        QuizSheetEvent quizSheetEvent = QuizSheetEvent.newQuizSheetModifiedEvent(quizSheet);
        eventPublisher.publishEvent(quizSheetEvent);
    }

    public void removeQuizSheet(String quizSheetId) {
        /* Autogen by nara studio */
        QuizSheet quizSheet = findQuizSheet(quizSheetId);
        quizSheetStore.delete(quizSheet);
        QuizSheetEvent quizSheetEvent = QuizSheetEvent.newQuizSheetRemovedEvent(quizSheet.getId());
        eventPublisher.publishEvent(quizSheetEvent);
    }

    public boolean existsQuizSheet(String quizSheetId) {
        /* Autogen by nara studio */
        return quizSheetStore.exists(quizSheetId);
    }

    public void handleEventForProjection(QuizSheetEvent quizSheetEvent) {
        /* Autogen by nara studio */
        switch(quizSheetEvent.getCqrsDataEventType()) {
            case Registered:
                quizSheetStore.create(quizSheetEvent.getQuizSheet());
                break;
            case Modified:
                QuizSheet quizSheet = quizSheetStore.retrieve(quizSheetEvent.getQuizSheetId());
                quizSheet.modifyValues(quizSheetEvent.getNameValues());
                quizSheetStore.update(quizSheet);
                break;
            case Removed:
                quizSheetStore.delete(quizSheetEvent.getQuizSheetId());
                break;
        }
    }
}
