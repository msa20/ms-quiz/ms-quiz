/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizSheet;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.microservice.quiz.aggregate.quizsheet.store.maria.jpo.QuizSheetJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;

@Getter
@Setter
@NoArgsConstructor
public class QuizSheetDynamicQuery extends CqrsDynamicQuery<QuizSheet> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<QuizSheetJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), QuizSheetJpo.class);
        TypedQuery<QuizSheetJpo> query = RdbQueryBuilder.build(request);
        QuizSheetJpo quizSheetJpo = query.getSingleResult();

        if (quizSheetJpo != null) {
            setQueryResult(quizSheetJpo.toDomain());
        } else {
            setQueryResult(null);
        }
    }
}
