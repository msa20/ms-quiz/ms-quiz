/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.domain.event;

import lombok.Getter;
import lombok.Setter;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.Quiz;
import java.lang.String;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
public class QuizEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private Quiz quiz;
    private String quizId;
    private NameValueList nameValues;

    protected QuizEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static QuizEvent newQuizRegisteredEvent(Quiz quiz) {
        /* Autogen by nara studio */
        QuizEvent event = new QuizEvent(CqrsDataEventType.Registered);
        event.setQuiz(quiz);
        return event;
    }

    public static QuizEvent newQuizModifiedEvent(String quizId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizEvent event = new QuizEvent(CqrsDataEventType.Modified);
        event.setQuizId(quizId);
        event.setNameValues(nameValues);
        return event;
    }

    public static QuizEvent newQuizModifiedEvent(Quiz quiz) {
        /* Autogen by nara studio */
        QuizEvent event = new QuizEvent(CqrsDataEventType.Modified);
        event.setQuizId(quiz.getId());
        event.setQuiz(quiz);
        return event;
    }

    public static QuizEvent newQuizRemovedEvent(Quiz quiz) {
        /* Autogen by nara studio */
        QuizEvent event = new QuizEvent(CqrsDataEventType.Removed);
        event.setQuizId(quiz.getId());
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static QuizEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, QuizEvent.class);
    }
}
