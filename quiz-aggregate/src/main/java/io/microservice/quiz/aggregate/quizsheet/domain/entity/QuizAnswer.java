package io.microservice.quiz.aggregate.quizsheet.domain.entity;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizAnswerCdo;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.*;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Setter
@Getter
@NoArgsConstructor
public class QuizAnswer extends DomainEntity {

    private String quizId;
    private String quizAnswerSheetId;

    private QuizGradingResult quizGradingResult;


    private String itemSeq;
    private IdName writer;

    public QuizAnswer(String id){
        super(id);
    }

    public QuizAnswer(QuizAnswerCdo quizAnswerCdo){
        super();
        BeanUtils.copyProperties(quizAnswerCdo,this);
    }

    public void modifyValues(NameValueList nameValues) {
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();

            switch (nameValue.getName()){
                case "itemSeq":
                    this.itemSeq = value;
                    break;
                case "quizGradingResult":
                    this.quizGradingResult = QuizGradingResult.valueOf(value);
                    break;
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }
}
