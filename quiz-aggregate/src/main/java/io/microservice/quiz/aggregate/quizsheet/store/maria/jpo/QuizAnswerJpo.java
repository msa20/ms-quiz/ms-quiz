/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.store.maria.jpo;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizGradingResult;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.store.jpa.DomainEntityJpo;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "QUIZ_ANSWER")
public class QuizAnswerJpo extends DomainEntityJpo {
    /* Autogen by nara studio */
    private String quizId;
    private String quizAnswerSheetId;
    @Enumerated(EnumType.STRING)
    private QuizGradingResult quizGradingResult; // io.naradrama.exam.aggregate.quiz.domain.entity.vo.QuizGradingResult
    private String itemSeq;
    private String writerId;
    private String writerName;

    public QuizAnswerJpo(QuizAnswer quizAnswer) {
        /* Autogen by nara studio */
        super(quizAnswer);
        BeanUtils.copyProperties(quizAnswer, this);

        this.writerId = quizAnswer.getWriter().getId();
        this.writerName = quizAnswer.getWriter().getName();
    }

    public QuizAnswer toDomain() {
        /* Autogen by nara studio */
        QuizAnswer quizAnswer = new QuizAnswer(getId());
        BeanUtils.copyProperties(this, quizAnswer);

        quizAnswer.setWriter(IdName.of(this.writerId, this.writerName));
        return quizAnswer;
    }

    public static List<QuizAnswer> toDomains(List<QuizAnswerJpo> quizAnswerJpos) {
        /* Autogen by nara studio */
        return quizAnswerJpos.stream().map(QuizAnswerJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }
}
