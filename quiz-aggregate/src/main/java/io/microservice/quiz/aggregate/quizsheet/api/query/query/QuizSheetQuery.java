/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizSheet;
import java.lang.String;
import io.microservice.quiz.aggregate.quizsheet.store.QuizSheetStore;

@Getter
@Setter
@NoArgsConstructor
public class QuizSheetQuery extends CqrsBaseQuery<QuizSheet> {
    /* Autogen by nara studio */
    private String quizSheetId;

    public void execute(QuizSheetStore quizSheetStore) {
        /* Autogen by nara studio */
        setQueryResult(quizSheetStore.retrieve(quizSheetId));
    }
}
