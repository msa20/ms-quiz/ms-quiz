package io.microservice.quiz.aggregate.quizsheet.domain.entity;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizAnswerSheetCdo;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class QuizAnswerSheet extends DomainEntity {
    private String groupId;

    private String quizSheetId;

    private IdName writer;
    private String subject;
    private Boolean passed;     // null 전체 풀이 전, true 전체 정답, false 오답

    transient private List<QuizAnswer> quizAnswers;

    public QuizAnswerSheet(String id){
        super(id);
    }

    public QuizAnswerSheet(QuizAnswerSheetCdo quizAnswerSheetCdo){
        super();
        BeanUtils.copyProperties(quizAnswerSheetCdo,this);
    }

    public void modifyValues(NameValueList nameValues) {

    }
}
