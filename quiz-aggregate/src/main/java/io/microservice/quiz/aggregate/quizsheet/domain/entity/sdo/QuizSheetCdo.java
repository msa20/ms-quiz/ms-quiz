package io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo;

import io.microservice.quiz.aggregate.quizsheet.domain.entity.Quiz;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizSheetCategory;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.vo.QuizSheetState;
import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.ddd.Enum;
import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class QuizSheetCdo implements JsonSerializable {
    //
    private String groupId;

    private String title;
    private String subject;
    private IdName writer;
    @Enum
    private QuizSheetCategory quizSheetCategory;
    private QuizSheetState QuizSheetState;

    transient private List<Quiz> quizzes;

    public String toString() {
        //
        return toJson();
    }

    public static QuizSheetCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, QuizSheetCdo.class);
    }
}
