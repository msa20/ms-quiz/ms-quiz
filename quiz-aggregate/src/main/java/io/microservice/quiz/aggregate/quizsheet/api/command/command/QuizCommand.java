/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizCdo;
import java.util.List;
import java.lang.String;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class QuizCommand extends CqrsBaseCommand {
    /* Autogen by nara studio */
    private QuizCdo quizCdo;
    private List<QuizCdo> quizCdos;
    private String quizId;
    private NameValueList nameValues;

    protected QuizCommand(CqrsBaseCommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static QuizCommand newRegisterQuizCommand(QuizCdo quizCdo) {
        /* Autogen by nara studio */
        QuizCommand command = new QuizCommand(CqrsBaseCommandType.Register);
        command.setQuizCdo(quizCdo);
        return command;
    }

    public static QuizCommand newRegisterQuizCommand(List<QuizCdo> quizCdos) {
        /* Autogen by nara studio */
        QuizCommand command = new QuizCommand(CqrsBaseCommandType.Register);
        command.setQuizCdos(quizCdos);
        return command;
    }

    public static QuizCommand newModifyQuizCommand(String quizId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizCommand command = new QuizCommand(CqrsBaseCommandType.Modify);
        command.setQuizId(quizId);
        command.setNameValues(nameValues);
        return command;
    }

    public static QuizCommand newRemoveQuizCommand(String quizId) {
        /* Autogen by nara studio */
        QuizCommand command = new QuizCommand(CqrsBaseCommandType.Remove);
        command.setQuizId(quizId);
        return command;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static QuizCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, QuizCommand.class);
    }
}
