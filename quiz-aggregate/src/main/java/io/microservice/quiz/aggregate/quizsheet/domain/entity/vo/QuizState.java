package io.microservice.quiz.aggregate.quizsheet.domain.entity.vo;

public enum QuizState  {
    //
    Working,
    Published
}
