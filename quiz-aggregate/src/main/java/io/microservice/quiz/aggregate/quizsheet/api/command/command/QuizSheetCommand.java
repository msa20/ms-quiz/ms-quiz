/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.api.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.sdo.QuizSheetCdo;
import java.util.List;
import java.lang.String;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class QuizSheetCommand extends CqrsBaseCommand {
    /* Autogen by nara studio */
    private QuizSheetCdo quizSheetCdo;
    private List<QuizSheetCdo> quizSheetCdos;
    private String quizSheetId;
    private NameValueList nameValues;

    protected QuizSheetCommand(CqrsBaseCommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static QuizSheetCommand newRegisterQuizSheetCommand(QuizSheetCdo quizSheetCdo) {
        /* Autogen by nara studio */
        QuizSheetCommand command = new QuizSheetCommand(CqrsBaseCommandType.Register);
        command.setQuizSheetCdo(quizSheetCdo);
        return command;
    }

    public static QuizSheetCommand newRegisterQuizSheetCommand(List<QuizSheetCdo> quizSheetCdos) {
        /* Autogen by nara studio */
        QuizSheetCommand command = new QuizSheetCommand(CqrsBaseCommandType.Register);
        command.setQuizSheetCdos(quizSheetCdos);
        return command;
    }

    public static QuizSheetCommand newModifyQuizSheetCommand(String quizSheetId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizSheetCommand command = new QuizSheetCommand(CqrsBaseCommandType.Modify);
        command.setQuizSheetId(quizSheetId);
        command.setNameValues(nameValues);
        return command;
    }

    public static QuizSheetCommand newRemoveQuizSheetCommand(String quizSheetId) {
        /* Autogen by nara studio */
        QuizSheetCommand command = new QuizSheetCommand(CqrsBaseCommandType.Remove);
        command.setQuizSheetId(quizSheetId);
        return command;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static QuizSheetCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, QuizSheetCommand.class);
    }
}
