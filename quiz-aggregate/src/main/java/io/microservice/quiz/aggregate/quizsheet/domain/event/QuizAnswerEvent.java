/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.microservice.quiz.aggregate.quizsheet.domain.event;

import lombok.Getter;
import lombok.Setter;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import io.microservice.quiz.aggregate.quizsheet.domain.entity.QuizAnswer;
import java.lang.String;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
public class QuizAnswerEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private QuizAnswer quizAnswer;
    private String quizAnswerId;
    private NameValueList nameValues;

    protected QuizAnswerEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static QuizAnswerEvent newQuizAnswerRegisteredEvent(QuizAnswer quizAnswer) {
        /* Autogen by nara studio */
        QuizAnswerEvent event = new QuizAnswerEvent(CqrsDataEventType.Registered);
        event.setQuizAnswer(quizAnswer);
        return event;
    }

    public static QuizAnswerEvent newQuizAnswerModifiedEvent(String quizAnswerId, NameValueList nameValues) {
        /* Autogen by nara studio */
        QuizAnswerEvent event = new QuizAnswerEvent(CqrsDataEventType.Modified);
        event.setQuizAnswerId(quizAnswerId);
        event.setNameValues(nameValues);
        return event;
    }

    public static QuizAnswerEvent newQuizAnswerModifiedEvent(QuizAnswer quizAnswer) {
        /* Autogen by nara studio */
        QuizAnswerEvent event = new QuizAnswerEvent(CqrsDataEventType.Modified);
        event.setQuizAnswerId(quizAnswer.getId());
        event.setQuizAnswer(quizAnswer);
        return event;
    }

    public static QuizAnswerEvent newQuizAnswerRemovedEvent(QuizAnswer quizAnswer) {
        /* Autogen by nara studio */
        QuizAnswerEvent event = new QuizAnswerEvent(CqrsDataEventType.Removed);
        event.setQuizAnswerId(quizAnswer.getId());
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static QuizAnswerEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, QuizAnswerEvent.class);
    }
}
